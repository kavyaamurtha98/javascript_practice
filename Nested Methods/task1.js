window.music = 'classical';
window.getMusic = function(){
	return this.music;
};
 
var foo = {
	music : 'jazz',
	getMusic : function(){
		return this.music;
	}
};
 
console.log(this.getMusic()); //'classical' (property of the window object)
console.log(foo.getMusic()); //'jazz' (property of the foo object)