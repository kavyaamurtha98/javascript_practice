//Declaring class
class Person
{
//Initiallizing an object    
    constructor(firstName, lastName, id, age)
    {
    this.firstName = firstName;
    this.lastName  = lastName;
    this.id = id;
    this.age =age; 
    }
//Declaring method
    details()
    {
        console.log(this.firstName + " " +this.lastName + " " +this.id + " " +this.age);
    }
}
//passing Object to a variable 
var _person =  new Person("kavya","gl",10,20);
console.log(_person.details()); //calling method

//kavya gl 10 20