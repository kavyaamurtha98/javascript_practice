class Student1
{
    constructor()
    {
        this.name = "kavya";
    }
}
class Student2 extends Student1
{
    constructor(id,age)
    {
        super(); //The super keyword is used to call the parnet class constructor
        this.id = id;
        this.age = age;
    }
}
var _student = new Student2(466,22);
console.log(_student.name+ " " +_student.id+ " " +_student.age);

//kavya 466 22