/*1. Create a class called 'fan', assume your own data members, methods atleast 6 each and two private data members.*/

class Fan{
    
    constructor( brand="usha", colour="red", hub=1, moutingDivce=1, blads=6, fanMotorSpeed=50, nuts=10, yoke=1, fanMotorScrews=30, hangingPins=4){
        var brand = brand;  //private data members
        var colour = colour;  //private data members
        this.hub = hub; 
        this.moutingDivce = moutingDivce;
        this.blads = blads;
        this.fanMotorSpeed = fanMotorSpeed;
        this.nuts = nuts;
        this.yoke = yoke;
        this.fanMotorScrews = fanMotorScrews;
        this.hangingPins = hangingPins;
    }
    calculateEstimate(){
        
        this.totalParts = this.hub + this.moutingDivce + this.blads + this.fanMotorSpeed + this.nuts + this.yoke + this.fanMotorScrews + this.hangingPins;
        return this.totalParts;
    }
 }
 
 var myFan = new Fan(); // Default values
 var _totalParts   = myFan.calculateEstimate()
 console.log(_totalParts);
 
 /*2.Create a child class 'tableFan' from above 'fan' */
 
    // Creating a new class from the parent
class tableFan extends Fan {
    constructor()
    {
        super();
        this.blads = 6;
    }
} 
     
var _tableFan = new tableFan();
console.log(_tableFan.blads);
   
 /* 3.Create a child class 'ceilingFan' from 'fan' */    
class ceilingFan extends Fan {
    constructor()
    {
        super();
        this.nuts = 10;
    }
}    
var _ceilingFan = new ceilingFan();
console.log(_ceilingFan.nuts);

/* 4. Try to access private members from 'fan' in 'ceilingFan'. Is this allowed?*/

class ceilingFans extends Fan{
    } 
  /*
   child: 'ceilingFans'
   parent: 'fan'
   */
var _ceilingFan = new ceilingFans();
console.log(ceilingFan. brand);


/*103
 6
 10
 undefined*/ 



   
