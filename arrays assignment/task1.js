//1. What happens when you access an element outside the length of the string with charAt?

var nameOfStudent = "amurtha"
//undefined
nameOfStudent 
//"amurtha"
nameOfStudent.length
//7
nameOfStudent.charAt(0)
//"a"
nameOfStudent.charAt(10)
//""