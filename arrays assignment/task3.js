//3. You can mutate an the value at an index in an array by using the square brackets. Does the
//same thing work with a string? Why might that be?

var suits=[]
var suits = ["clubs", "diamonds", "hearts", "spades"];
//undefined
suits
//(4) ["clubs", "diamonds", "hearts", "spades"]
suits[4]="sliver";
//"sliver"
suits[5]="gold";
//"gold"
suits
//(6) ["clubs", "diamonds", "hearts", "spades", "sliver", "gold"]
suits[2]="glass"
//glass
suits
//(6) ["clubs", "diamonds", "glass", "spades", "sliver", "gold"]
console.log(suits)
// (6) ["clubs", "diamonds", "glass", "spades", "sliver", "gold"]