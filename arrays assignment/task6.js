//6.Consider below array: var alphabets = ['A100', 'A', 'C200', 'D001', 'Z100', 'z001', 'X900', 'T900',
//'U98', 'a100'] Sort them in ascending order.

var alphabets = ['A100', 'A', 'C200', 'D001', 'Z100', 'z001', 'X900', 'T900','U98','a100'];
alphabets.sort()

//(10) ["A", "A100", "C200", "D001", "T900", "U98", "X900", "Z100", "a100", "z001"]