//2. What is the difference between a method & a function ?

// A function is a piece of code that is called by name. It can be passed data to operate on and can optionally return data. All data that is passed to a function is explicitly passed.

// A method is a piece of code that is called by a name that is associated with an object. In most respects it is identical to a function except for two key differences:

// 1.A method is implicitly passed the object on which it was called.
// 2.A method is able to operate on data that is contained within the class