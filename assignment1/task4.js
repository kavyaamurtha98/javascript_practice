// 4.We've seen that variables can store strings and numbers. We've also seen that when we have two
// strings the +operator means concatenation, whereas when we have two number variables it
// represents addition. What happens when we mix and match number and string variables? Give it a
// try.

var tweet = "hello world!";
        var count = 0;

        count = count + 10;
        tweet = tweet + count;

        // what is the value of tweet and count now?
                Ans: 
                    tweet = hello world!10
                    count = 10
        
        count = count * 10;
        tweet = count + tweet;

        // what is the value of tweet and count now?
                Ans: 
                     tweet = 100hello world!10
                     count = 100
        
        count = count * 100;
        tweet = tweet + ". this is another sentence.";
        
        // what is the value of tweet and count now?
                Ans:
                    tweet = "100hello world!10. this is another sentence."
                    count = 10000