// 5.Using the Chrome JavaScript console, practice with the typeof operator. What are the types of
// the following values?

typeof true;
//"boolean"
typeof "true";
//"string"
typeof 1;
//"number"
typeof "1";
//"string"
typeof "one";
//"string"