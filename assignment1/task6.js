// 6.Using the Chrome JavaScript console, practice with string methods. If you type a string and then
// the dot operator, and then wait a moment, you'll see that Chrome's auto-complete box will
// appear. You can use the up and down arrows to cycle through all the methods available on the
// string object. Apply a few of those methods and see if you can understand what they do.

var name ="kavya"
//undefined
name.length
//5
name.charAt(4)
//"a"
name.concat("amurtha")
//"kavyaamurtha"
name.toLocaleLowerCase()
//"kavya"
name.toLocaleUpperCase()
//"KAVYA"
name.anchor()
//"<a name="undefined">kavya</a>"