//1. How you will access static method from a child class (the static method is in a parent class)

class Student1 {
    static staticMethod() {
        return 'static method has been called .';
    }
}

class Student2 extends Student1 {
    
} 
Student2.staticMethod()  


//"static method has been called ."


