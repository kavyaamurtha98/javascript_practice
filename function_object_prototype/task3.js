//3. Explain closure. Write an example using a cell phone.

//closure is a self execting functions
//closure is an inner function that has access to outer function variables

var  cellPhone = [
    {"name" : "Nokia", "cost" : 15000},
    {"name" : "Oppo", "cost" : 10000},
  ];
  var cellPhoneName = cellPhone.filter(value => {
    if (value.cost == 15000)
    {
      return value.name;
    }
  }).map(element => element.name);
  
  console.log(cellPhoneName);
  
  // ["Nokia"]


