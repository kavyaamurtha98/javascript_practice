//Example1

const arr = [1,2,3,4,5,6,7];
const newArr =arr.map(a => a%2 ===0);
console.log(newArr);

//false, true,false, true,false, true,false

//Example2

const arr = [1,2,3,4,5,6,7];
const newArr =arr.map(a => a*a);
console.log(newArr);

//  1,  4,  9, 16,25, 36, 49