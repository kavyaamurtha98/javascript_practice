//3. How many times does the ace of spades appear? What about the two of clubs?

console.log(cards.length)
var uniqueArray = cards.filter(suits => suits.suit).map(element=>element.suit);
console.log(array)

var rankArray = cards.filter(rankValues => rankValues.rank).map(element=>element.rank);
console.log(rankArray)

var occurances ={};
var currentVal =0;
var maxValue = array[0];

function mostFrequentSuitValue(array)
{
        for(let i=0; i<array.length; i++)
        {
                currentVal = array[i];
                if(occurances[currentVal]==undefined)
                {
                        occurances[currentVal]= 1;
                }
                else
                {
                        occurances[currentVal]++;
                }
                if(occurances[currentVal] > occurances[maxValue])
                {
                maxValue = currentVal;
                }
        }
        return maxValue;
}
var mostFrequentlySuit = mostFrequentSuitValue(array);
console.log(occurances)
console.log("The spades sre : " + occurances.spades);
console.log("The clubs are : " + occurances.clubs);

var counts = {};
var compareCurrentVal = 0;
var countAce = rankArray[0]
function countAceValues(rankArray)
{
        for(let i=0; i<rankArray.length; i++)
                {
                        compareCurrentVal = rankArray[i];
                        if(counts[compareCurrentVal]==undefined)
                        {
                                counts[compareCurrentVal]= 1;
                        }
                        else
                        {
                                counts[compareCurrentVal]++;
                        }

                        if(counts[compareCurrentVal] > counts[countAce])
                        {
                                countAce = compareCurrentVal;
                        }
                }
        return countAce;
}        
var aceValue = countAceValues(rankArray);
console.log("The ace are : " + counts.ace);
/*
5000
[
  'clubs',    'diamonds', 'diamonds', 'clubs',    'hearts',   'hearts',
  'diamonds', 'spades',   'clubs',    'spades',   'hearts',   'spades',
  'clubs',    'hearts',   'diamonds', 'clubs',    'diamonds', 'diamonds',
  'hearts',   'spades',   'hearts',   'diamonds', 'diamonds', 'hearts',
  'spades',   'clubs',    'clubs',    'spades',   'diamonds', 'hearts',
  'clubs',    'hearts',   'hearts',   'spades',   'hearts',   'diamonds',
  'clubs',    'hearts',   'diamonds', 'hearts',   'hearts',   'spades',
  'spades',   'clubs',    'clubs',    'hearts',   'clubs',    'diamonds',
  'spades',   'spades',   'clubs',    'hearts',   'diamonds', 'clubs',
  'clubs',    'hearts',   'diamonds', 'diamonds', 'spades',   'spades',
  'spades',   'diamonds', 'spades',   'hearts',   'clubs',    'spades',
  'diamonds', 'diamonds', 'clubs',    'clubs',    'diamonds', 'diamonds',
  'hearts',   'spades',   'diamonds', 'hearts',   'clubs',    'diamonds',
  'clubs',    'clubs',    'hearts',   'spades',   'hearts',   'clubs',
  'spades',   'spades',   'clubs',    'clubs',    'diamonds', 'clubs',
  'diamonds', 'clubs',    'spades',   'clubs',    'spades',   'diamonds',
  'clubs',    'spades',   'diamonds', 'hearts',
  ... 4900 more items
]
[
  'six',   'jack',  'eight', 'ace',   'three', 'three', 'six',
  'king',  'nine',  'five',  'six',   'ace',   'ten',   'five',
  'seven', 'six',   'queen', 'ace',   'five',  'nine',  'nine',
  'king',  'four',  'jack',  'queen', 'two',   'six',   'king',
  'queen', 'ace',   'two',   'six',   'eight', 'nine',  'jack',
  'queen', 'ace',   'four',  'six',   'jack',  'three', 'queen',
  'king',  'seven', 'three', 'five',  'jack',  'queen', 'six',
  'ten',   'eight', 'two',   'queen', 'nine',  'four',  'queen',
  'two',   'four',  'jack',  'jack',  'eight', 'eight', 'king',
  'seven', 'two',   'nine',  'king',  'ten',   'four',  'seven',
  'nine',  'queen', 'queen', 'eight', 'king',  'four',  'ten',
  'eight', 'three', 'six',   'ten',   'nine',  'queen', 'eight',
  'seven', 'two',   'queen', 'queen', 'four',  'jack',  'three',
  'queen', 'queen', 'ten',   'six',   'ace',   'nine',  'ten',
  'jack',  'three',
  ... 4900 more items
]
{ clubs: 1238, diamonds: 1252, hearts: 1257, spades: 1253 }
The spades sre : 1253
The clubs are : 1238
The ace are : 402 */

