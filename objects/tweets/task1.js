// 1. Create an array that only contains only the tweet texts that contain the word "awesome" (upper
// or lower case). How many tweets are in the array?

console.log("The total tweets is : " + tweets.length)

const text = tweets.filter(function(elementValue)
{
    return elementValue.text.toUpperCase().indexOf("AWESOME") != -1;
}).length
    
console.log("The AWESOME text tweets are in the array is " + text);

// The total tweets is : 500
//The AWESOME text tweets are in the array is 3
